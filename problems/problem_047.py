# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    password = format(password)
    pass_len = range(len(password))
    total_len = pass_len[-1] + 1
    
    count_upper = 0
    for i in password:
        if i.isupper():
            count_upper = count_upper + 1
        else:
            count_upper = count_upper
            
    count_lower = 0
    for i in password:
        if i.islower():
            count_lower = count_lower + 1
        else:
            count_lower = count_lower
    
    count_digit = 0
    for i in password:
        if i.isdigit():
            count_digit = count_digit + 1
        else:
            count_digit = count_digit
    
    count_spec = 0
    for i in password:
        if i == "$" or "!" or "@":
            count_spec = count_spec + 1
        else:
            count_spec = count_spec
    


    if (
        
        total_len >= 6
        and total_len <= 12
        and count_lower >= 1
        and count_upper >= 1
        and count_digit >= 1
        and count_spec >= 1
        ):
            return 'good'
        
    else:
        return 'bad'


